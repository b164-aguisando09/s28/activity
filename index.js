fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
	console.log(data)
	let title = data.map(function(data){
		return data.title;
	})
	console.log(title);
})



fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
	let singleTitle = data.filter(function(data){
		return (data.title == "delectus aut autem");
	})

	console.log(singleTitle);
	console.log(`The item "${singleTitle[0].title}" on the list has a status of ${singleTitle[0].completed}.`);
})

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Create a To Do List Item.',
		id: 201,
		userId: 1,
		completed: false
	})
})
.then(response => response.json())
.then(json => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated to do list item",
		description: "To update the my to do list with a different data structure",
		userId: 1,
		dateCompleted: "Pending",
		status: "Pending"
	})
})
.then(res => res.json())
.then(data => console.log(data));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete"

	})
})
.then(res => res.json())
.then(data => console.log(data));

fetch('https://jsonplaceholder.typicode.com/todos/5', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))

